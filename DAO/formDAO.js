const  mongoose= require('mongoose')
var form= require('../models/forms');
const {ObjectId} = require("bson");
class formDAO {
    constructor() {
    }

    searchallForms(req, res, next) {
        form.find()
            .then(forms => res.json(forms))
            .catch(err => console.log(err));
        return;
    };

// Search form
    SearchForm(req, res, next) {
        console.log("hiiiiiiiiiii");
        form.findById(req.params.id)
            .then((forms) => {
                if (!forms) {
                    return res.status(404).send({
                        message: "Form not found with id " + req.params.id,
                    });
                }
                res.status(200).send(forms);
                console.log(forms);
            })
            .catch((err) => {
                return res.status(500).send({
                    message: "Error retrieving form with id " + req.params.id,
                });
            });

        return;
    };


//  add form
    addform(req, res, next) {
        console.log(req.body);
        /**
         * validation request
         */
        if (!req.body.titleForm) {
            return res.status(400).send({
                message: "Required field can not be empty",
            });
        }
        /**
         * Create a form
         */
        var Form= new form({
            titleForm:          req.body.titleForm,
            description:        req.body.description,
            dateCreation :      req.body.dateCreation,
            items:              req.body.items
        });
        /**
         * Save form to database
         */
        Form.save().then(result => {
            res.send('ok')


        })
    };


    updateForm(req, res, next) {

        form.update(
            {
                titleForm:          req.body.titleForm,
                description:        req.body.description,
                dateCreation :      req.body.dateCreation,
                items:              req.body.items
            },
            {
                where: {
                    _id: req.params._id
                }
            }
        ).then(function (rowsUpdated) {
            res.json(rowsUpdated)
        })
            .catch(next);
        return;
    };



    deleteForm(req, res, next) {

        var id = req.params.id;
        console.log(id)
        form.deleteOne( {_id: ObjectId(id)})
            .then(result => {
                res.send('ok')
            });

    }

}


module.exports = formDAO;