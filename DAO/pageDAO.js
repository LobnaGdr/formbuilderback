const  mongoose= require('mongoose')
var page= require('../models/pages');
const {ObjectId} = require("bson");
class pageDAO {
    constructor() {
    }

    searchallpages(req, res, next) {
        page.find()
            .then(pages => res.json(pages))
            .catch(err => console.log(err));
        return;
    };

// Search page
    SearchPage(req, res, next) {
        page.findById(req.params.id)
            .then((pages) => {
                if (!pages) {
                    return res.status(404).send({
                        message: "page not found with id " + req.params._id,
                    });
                }
                res.status(200).send(pages);
                console.log(pages);
            })
            .catch((err) => {
                return res.status(500).send({
                    message: "Error retrieving page with id " + req.params._id,
                });
            });

        return;
    };


//  add page
    addPage(req, res, next) {
        console.log(req.body);
        /**
         * validation request
         */
        if (!req.body.titlePage) {
            return res.status(400).send({
                message: "Required field can not be empty",
            });
        }
        /**
         * Create a page
         */
        var pages= new page({
            titlePage:          req.body.titlePage,
            description:        req.body.description,
            Form:             req.body.Form,
            Responses : null
        });
        console.log('pages', pages);
        /**
         * Save page to database
         */

        pages.save().then(result => {
            console.log("result", result);
            res.send(result)


        })
    };


    updatePage(req, res, next) {

        page.update(
            {
                _id: req.params._id
            },
            {
                Responses:          req.body.Responses,

            }
        ).then(function (rowsUpdated) {
            res.json(rowsUpdated)
        })
            .catch(next);
        return;
    };



    deletePage(req, res, next) {

        var id = req.params.id;
        console.log(id)
        page.deleteOne( {_id: ObjectId(id)})
            .then(result => {
                res.send('ok')
            });


    }

}


module.exports = pageDAO;