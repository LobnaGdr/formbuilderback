const mongoose = require('mongoose');
const UserSchema = mongoose.Schema ({
      userFristName:   {type: String, required: true},
      userLastName:    {type: String, required: true},
      userSexe :       String,
      userAdreesEmail: {type: String, required: true},
      userLogin:       {type: String, required: true},
      userPassword:    {type: String, required: true}
});
module.exports = mongoose.model('users', UserSchema);
