'use strict';
var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController');
var formController = require('../controllers/formController');
var pageController = require('../controllers/pageController');
var responseCotroller = require('../controllers/responseController');
//root user
router.post('/user/add',userController.addUser);
router.post('/user/search/:id',userController.searchUser);
router.get('/user/searchall', userController.searchallUsers);
router.put('/update/:id', userController.updateUser);
router.put('/user/updateuserps/:id', userController.updatePassword);
router.post('/user/auth', userController.auth);
router.delete('/user/delete/:id', userController.deleteUser);
//root forms
router.post('/forms/add',formController.addForm);
router.get('/forms/search/:id',formController.searchForm);
router.get('/forms/searchall', formController.searchallForms);
router.put('/forms/update/:id', formController.updateForm);
router.delete('/forms/delete/:id', formController.deleteForm);

//root pages
router.post('/pages/addPage',pageController.addPage);
router.get('/pages/search/:id',pageController.searchPage);
router.get('/pages', pageController.searchallPages);
router.put('/pages/update/:id', pageController.updatePage);
router.delete('/pages/delete/:id', pageController.deletePage);
//root response


router.post('/response/add',responseCotroller.addresponse);
router.get('/response/search/:id',responseCotroller.searchresponse);
router.get('/response/searchall', responseCotroller.searchallresponses);
router.get('/response/form/:idForm', responseCotroller.searchByForm);
router.get('/response/pages/:idPage', responseCotroller.searchByPage);





module.exports = router;